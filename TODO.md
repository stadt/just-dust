# todo

* color class for:
  * cv2 color representations
  * hsv/hls and rgb(a)
  * fancy color addition (average saturation/value and wrap hue)
* balance between inelastic and elastic collisions
* argparsing
* real particles (protons, neutrons, electrons) with elastic collisions